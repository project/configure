<?php
/**
 * @file
 * Administration pages for Configuration module.
 */

/**
 * Form constructor for the variables list.
 */
function configure_admin_list_form() {
  $header = array(
    'name' => t('Name'),
    'value' => t('Value'),
    'operations' => t('Operations'),
  );

  $select = db_select('configure', 'c')
    ->fields('c', array('name', 'value'));

  $result = $select->execute();

  foreach ( $result as $row ) {
    $rows[$row->name] = array(
      'name' => $row->name,
      'value' => strlen($row->value) <= 75 ? $row->value : substr($row->value, 0, 50) . ' …',
      'operations' => l(t('export'), "admin/config/variables/export/{$row->name}"),
    );
  }

  $form['variables'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => !empty($rows) ? $rows : array(),
    '#empty' => t('No variables have been saved'),
  );

  $form['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export selected variables'),
  );

  return $form;
}

/**
 * Submit handler for configure_admin_list_form().
 */
function configure_admin_list_form_submit(&$form, &$form_state) {
  $variables = array_filter($form_state['values']['variables']);
  $variables = urlencode(implode(',', $variables));
  $form_state['redirect'] = "admin/config/variables/export/$variables";
}

/**
 * Exports variables in a text area as JSON.
 */
function configure_admin_export_form($form, &$form_state, $variable = NULL) {


  $select = db_select('configure', 'c')
    ->fields('c', array('name', 'value'));

  if ( !empty($variable) ) {
    $variables = explode(',', urldecode($variable));
    $select->condition('name', $variables);
  }
  else {
    drupal_set_title(t('Export all variables'));
  }

  $result = $select->execute();
  if ( $result->rowCount() <> 0 ) {
    foreach ( $result as $row ) {
      $variable_values[$row->name] = unserialize($row->value);
    }
  }

  $variables_json = !empty($variable_values) ? drupal_json_encode($variable_values) : '';

  $form['export'] = array(
    '#type' => 'textarea',
    '#rows' => 30,
    '#title' => t('Variables'),
    '#default_value' => $variables_json,
  );

  return $form;
}

/**
 * Form constructor for variable-import page.
 */
function configure_admin_import_form() {
  $form['import'] = array(
    '#type' => 'textarea',
    '#rows' => 30,
    '#title' => t('Variables'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import variables'),
  );

  return $form;
}

/**
 * Submit handler for configure_admin_import_form().
 */
function configure_admin_import_form_submit(&$form, &$form_state) {
  $variables = drupal_json_decode($form_state['values']['import']);

  if ( !empty($variables) ) {
    foreach ( $variables as $name => $value ) {
      // Delete existing, to prevent exceptions
      $delete = db_delete('configure')
        ->condition('name', $name)
        ->execute();

      $record = array(
        'name' => $name,
        'value' => serialize($value),
      );

      drupal_write_record('configure', $record);
    }
  }
}
