<?php
/**
 * @file
 *  Hooks provided by the Config module.
 *
 * @todo Add documentation for reference variables.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines variables for a module.
 *
 * @return array
 *   An array whose keys are variable names and whose values are arrays
 *   describing the variable, with the following key/value pairs:
 *   - type: The type of the variable. Defaults to 'linear'. Currently supported
 *   types are:
 *     - linear: a single-value variable, stored as a single record in the
 *     database
 *     - meta: a single-value variable, whose name must be in the form
 *     'variable_name:identifier', where the identifier makes the variable
 *     a unique record in the database. Meta variables should be set by modules
 *     programmatically, and not be exposed for editing to configure_settings_form().
 *     Avoid providing an 'element' item to meta variables
 *     - group: an array of single-value variables, stored as a single record in
 *     the database
 *     - matrix: an array of multiple-value variables. It requires the 'properties'
 *     item.
 *   - group: A machine-readable name used to group variables together. Defaults to
 *   'general'.
 *   - main module: (optional) @todo.
 *   - title: (optional) A human-readable, translatable name for the variable. Must
 *   be wrapped in t().
 *   - description: (optional) A short description for the variable, if needed. Must
 *   be wrapped in t().
 *   - default: (optional) The default value of the variable. The default values for group
 *   and matrix variables should be set for each of the variable's items.
 *   - 'edit link': (optional) Used specifically for matrix variables, it's the path to
 *   the page where each item of a matrix variable can be edited. Each module should provide
 *   the specific menu item (of MENU_CALLBACK type), which calls configure_variable_edit_form().
 *   - 'options callback': (optional) The name of a function (defined by the module) that
 *   must return an array of values used to populate the variable's (or the variable's
 *   items') element's '#options' array, where necessary.
 *   - default identifier: Used only for meta variables, it's the default value of the
 *   identifier portion of the variable name.
 *   - items: An array whose keys are machine-readable sub-variable names, and whose values
 *   are arrays describing each item, with a structure similar to that of linear variables.
 *   In particular, each item can define:
 *     - title
 *     - default: in the case of matrix variables, the default should be an array whose keys
 *     are the names of the variable's defined properties (see properties below), and
 *     whose values are the default values for each property
 *   - element: An array that will be used to render the variable's edit field
 *   in the user interface. The element set for group variables will be applied to all
 *   items in the variable. Do not use for meta variables or for matrix variables. Matrix
 *   variables set specific elements for each of their properties. The element's '#type'
 *   defaults to 'textfield', so it can be omitted.
 *   - properties: An array of properties that will be used to build a matrix variable's
 *   single items. Its keys are machine-readable property names, and its values
 *   are arrays describing each item, with a structure similar to that of linear variables.
 *   In particular, each item can define:
 *     - title
 *     - default
 *     - element
 *     - options callback.
 *
 * @see hook_configure_variable_info_alter()
 * @see configure_settings_form()
 * @see configure_variable_edit_form()
 */
function hook_configure_variable_info() {

}

/**
 * Allows modules to alter variables defined by other modules.
 */
function hook_configure_variable_info_alter(&$variables) {

}

/**
 * Allows modules to provide default values for variables.
 *
 * @return array
 *   An associative array whose keys are variable names, and whose values are the
 *   default values of each variable, of whichever data type is required by the variable
 *   definition.
 */
function hook_configure_variable_defaults() {

}

/**
 * Allows modules to alter defaults.
 * Especially useful when variables have been exported as features.
 */
function hook_configure_variable_defaults_alter(&$defaults) {

}

/**
 * Allows modules to alter the value of a variable before it's set.
 * Can lead to unexpected results, so use sparingly.
 */
function hook_configure_variable_set_value_alter($variable_name, &$value) {

}

/**
 * Allows modules to perform actions after a variable is saved to the config table.
 * This doesn't affect the value of the variable.
 *
 * @param $variables
 *   An array with the following key/value pairs:
 *   - name: the name of the variable, as defined by hook_configure_variable_info().
 *   - value: the unserialized value of the variable.
 *   - options: an array of options passed to configure_variable_set().
 *   - variable_name: the name of the variable as found in the config table. Different
 *   from 'name' only in the case of meta variables.
 *
 * @see configure_variable_set()
 * @see hook_configure_variable_info()
 */
function hook_configure_variable_set($variables) {

}

/**
 * Allows modules to return the context (i.e. the identified variable, regardless
 * of type) of a meta variable.
 *
 * @param $name
 *   The name of the variable, without the identifier name.
 *
 * @see configure_get_identifier_context()
 */
function hook_configure_variable_identifier($name, $identifier) {

}
/**
 *@} End of "addtogroup hooks".
 */