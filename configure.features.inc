<?php
/**
 * @file
 * Features integration for Configure module. WIP and largely not functional.
 */
/**
 * Implements hook_features_export().
 */
function configure_features_export($data, &$export, $module_name = '') {
	$export['features']['config'] = $data;

	return array();
}

/**
 * Implements hook_features_export_options().
 */
function configure_features_export_options() {
	$variables = configure_variable_get_info();
	$titles = configure_variable_get_title();
	$return = array();

	foreach ( $variables as $variable => $definition ) {
		switch ( $definition['type'] ) {
			case 'meta' :
				// Get available meta variables
				$values = cvget($variable, array($definition['identifier'] => '%'));

				if ( !empty($values) ) {
					if ( isset($values['multiple']) ) {
						$values = $values['multiple'];
					}
					else {
						// If there's only one result, reconstruct the array
						$values = array(
							$variable = $values
						);
					}

					foreach ( $values as $identifier => $value ) {
						$context = configure_get_identifier_context($definition, $identifier);

						// Produce a title specific for every instance of the meta variable
						$return[$variable . ':' . $identifier] = t('@variable: @identifier', array('@variable' => $titles[$variable], '@identifier' => $context['title']));
						unset($identifier, $context);
					}
				}
			break;

			default :
				$return[$variable] = $titles[$variable];
			break;
		}
	}

	asort($return);

	return $return;
}

function configure_features_export_render($module, $data, $export = NULL) {
	$variables = configure_variable_get_info();
	$output = array();

	// Query database directly to make it quicker
	// and to avoid returning defaults
	$query = db_select('config', 'c');
	$query->fields('c')
		->condition('name', array_keys($data));
	$result = $query->execute();

	foreach ( $result as $row ) {
		$name = $row->name;
		$value = unserialize($row->value);

		// Explode the variable name to account for meta variables
		$name = explode(':', $name);

		if ( isset($name[1]) ) {
			$identifier = $name[1];
			$name = $name[0];
		}
		else {
			$name = $name[0];
		}

		$definition = $variables[$name];

		$options = array();

		switch ( $definition['type'] ) {
			case 'meta' :
				// $identifier_name = $definition['default identifier'];
				// $options = array(
				// 	$identifier_name => $identifier
				// );

				// The defaults for meta will include the identifier, as they're not supposed
				// to be set directly by modules
				// The value is serialized, because the type and depth of the data are uncertain
				// (Yes, it reserializes unserialized data, but that's what it is.)
				$value = serialize($value);

				$output[] = '  $defaults' . "['{$name}']['multiple']['{$identifier}'] = array(";
				$output[] = "    'serialized' => '{$value}',";
				$output[] = "  );";
			break;

			case 'group' :
				$output[] = '  $defaults' . "['{$name}'] = array(";

				// Default for every element of the group
				foreach ( $value as $element_key => $element_value ) {
					$output[] = "    '{$element_key}' => '{$element_value}',";
				}

				$output[] = "  );";
			break;

			case 'matrix' :
				$output[] = '  $defaults' . "['{$name}'] = array(";
				// Find matrix properties
				$properties = array_keys($definition['properties']);
				foreach ( $value as $element_key => $element_value ) {
					$output[] = "    '{$element_key}' => array(";

					foreach ( $properties as $property ) {
						if ( !empty($element_value[$property]) ) {
							$output[] = "      '{$property}' => '{$element_value[$property]}',";
						}
					}

					$output[] = "    ),";
				}

				$output[] = "  );";
			break;

			// Literal variables
			default :
				$output[] = '  $defaults' . "['{$name}'] = '{$value}';";
			break;
		}
	}

	$output = implode("\n", $output);
	return array(
		'configure_variable_defaults_alter' => array(
			'code' => $output,
			'args' => '&$defaults'
		)
	);
}