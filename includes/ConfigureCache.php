<?php

/**
 * Class ConfigureCache
 */
class ConfigureCache {

  /**
   * The cache bin.
   *
   * @var string
   */
  public static $bin = 'cache_configure';

  /**
   * Gets the value of a cached configuration variable.
   *
   * @param $name
   *
   * @return |null
   */
  public static function get($name) {
    $cache = cache_get($name, static::$bin);
    return !empty($cache->data) ? $cache->data : NULL;
  }

  /**
   * Caches the value of a configuration variable.
   *
   * @param $name
   * @param $data
   * @param $definition
   *
   * @return mixed
   */
  public static function set($name, $data, $definition) {
    if (isset($definition['cache expiration'])) {
      $expire = REQUEST_TIME + $definition['cache expiration'];
    }
    else {
      $expire = CACHE_PERMANENT;
    }

    cache_set($name, $data, static::$bin, $expire);
    return $data;
  }

  /**
   * Deletes the cached value of a configuration variable.
   * @param $name
   */
  public static function clear($name) {
    $object = _cache_get_object(static::$bin);
    $object->clear($name);
  }
}